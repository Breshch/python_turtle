import turtle
turtle.shape('turtle')

step = 0
while step < 360:
    turtle.forward(step)
    turtle.right(step)
    step += 0.01

turtle.done()\

