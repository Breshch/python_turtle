import turtle
turtle.shape('turtle')

for i in range(10000):
    turtle.forward(i * 0.01)
    turtle.left(5)

turtle.done()
