import turtle
turtle.shape('turtle')

for i in range(10000):
    turtle.forward(i)
    turtle.left(90)
    i += 1

turtle.done()