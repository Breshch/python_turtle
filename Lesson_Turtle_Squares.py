import turtle
turtle.shape('turtle')

count_of_squares = 0
angle = 90
step = length = 10

for count_of_squares in range(0, 11, 1):
    for count_of_moves in range(0, 4, 1):
        turtle.forward(length)
        turtle.left(angle)
    turtle.penup()
    turtle.setx(-10 * (count_of_squares + 1))
    turtle.sety(-10 * (count_of_squares + 1))
    turtle.pendown()
    length = 2 * step + length

turtle.done()

