import turtle
turtle.shape('turtle')

count_of_legs = 12
angle = 360/count_of_legs
length = 100
step = 0

while step < count_of_legs:
    turtle.right(angle)
    turtle.forward(length)
    turtle.stamp()
    turtle.backward(length)
    step += 1

turtle.done()
